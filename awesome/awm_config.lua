awm_config = {}

--[[
    Config file for awesomewm
    Edit this file to configure your awesomewm rather than rc.lua
]]

local awful = require("awful")
local wibox = require("wibox")

--[[
    You can set your prefered applications here
    You can add more to the list by creating a new variable
    Variable name can be anything complying with LUA variable naming policy
    Later you can use variables defined in here for setting up keybinds and other things
    Keep in mind when making keybinds that if the program / script is in your $PATH you should use
        awful.util.spawn( variable_name )
    and
        awful.spawn.with_shell( variable_name )
    otherwise.
]]

_G.terminal = "xfce4-terminal"
_G.editor = os.getenv("EDITOR") or "vim"
_G.editor_cmd = terminal .. " -e " .. editor
_G.web_browser = "qutebrowser"
_G.separator = wibox.widget.textbox(' | ')
_G.file_manager = "pcmanfm"
_G.crypto_wallet = "exodus"

--[[
    Set the name of your chosen theme here
    You do not need to specify the path here
    Only give the name of the chosen theme
    Name of the theme is the name of the folder located in themes folder
]]

_G.theme_name = "modtheme"

--[[ Default modkey.
    Usually Mod4 is the key with a logo between Control and Alt (Superkey)
    If you do not like this or do not have such a key I suggest you to remap Mod4 to another key using xmodmap or other tools
    However you can use another modifier like Mod1 but it may interact with others
]]

_G.modkey = "Mod4" -- Superkey

--[[
    List of layouts available
    To enable any layout uncomment it by removing -- in front of the layout name
    To disable any layout comment it out by adding -- in front of the layout name
    Deleting the line entirely is not recommended in case you change your mind at any stage
    Order in which layouts are given does matter
    In case you want to rearange them to fit your needs feel free to do so
]]

_G.available_layouts = {
                        --awful.layout.suit.floating,
                        awful.layout.suit.tile,
                        awful.layout.suit.tile.left,
						--awful.layout.suit.tile.bottom,
                        awful.layout.suit.tile.top,
                        awful.layout.suit.fair,
                        awful.layout.suit.fair.horizontal,
						--awful.layout.suit.spiral,
						--awful.layout.suit.spiral.dwindle,
						--awful.layout.suit.max,
						--awful.layout.suit.max.fullscreen,
						--awful.layout.suit.magnifier,
						--awful.layout.suit.corner.nw,
						--awful.layout.suit.corner.ne,
						--awful.layout.suit.corner.sw,
						--awful.layout.suit.corner.se,
}

--[[
    Tags list
]]

_G.List_of_tags = {
                    "HOME",
                    "WWW",
                    "MINING",
                    "PROGRAMMING",
                    "SSH",
                    "VIRTUALBOXES",
                    "GAMING",
                    "ANDROID",
                    "STREAM"
}

return awm_config
